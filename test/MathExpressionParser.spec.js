/*
global require
*/

const expect = require("chai").expect;
const MathExpressionParser = require("../build/Parser/MathExpressionParser").MathExpressionParser;

const mathExpressionParser = new MathExpressionParser();

const toLevelOrder = expressionStr => mathExpressionParser.parseInfixExpression(expressionStr).traverseToPrint.exportLevelOrder().map(node => node.key).join(' ');
const toPreOrder = expressionStr => mathExpressionParser.parseInfixExpression(expressionStr).traverseToPrint.exportPreOrder().map(node => node.key).join(' ');
const toInOrder = expressionStr => mathExpressionParser.parseInfixExpression(expressionStr).traverseToPrint.exportInOrder().map(node => node.key).join(' ');
const toPostOrder = expressionStr => mathExpressionParser.parseInfixExpression(expressionStr).traverseToPrint.exportPostOrder().map(node => node.key).join(' ');

const parseAndEvaluate = expressionStr => mathExpressionParser.parseInfixExpression(expressionStr).traverseToEvaluateMathExpression.solve();
const isExpressionValid = expressionStr => mathExpressionParser.parseInfixExpression(expressionStr).traverseToEvaluateMathExpression.verify();
const isExpressionComplete = expressionStr => mathExpressionParser.parseInfixExpression(expressionStr).traverseToEvaluateMathExpression.validate();
const isExpressionValidButIncomplete = expressionStr => isExpressionValid(expressionStr) && !isExpressionComplete(expressionStr);

describe('MathExpressionParser', () => {

    it('should treat special characters as part of operand', () => {
        let result = mathExpressionParser.splitToken(' 1234567890~!@#$%_`={}[]\:";\'<>?,.');
        expect(result.length).to.equal(1);
    });

    it('should invalidate invalid operand', () => {
        expect( isExpressionValid('123') ).to.equal(true);
        expect( isExpressionValid(' 123') ).to.equal(true);
        expect( isExpressionValid('1 23') ).to.equal(false);
        expect( isExpressionValid('12 3') ).to.equal(false);
        expect( isExpressionValid('123 ') ).to.equal(true);
        expect( isExpressionValid(' 123 ') ).to.equal(true);
    });

    it('should invalidate an empty expression', () => {
        expect( isExpressionValid('') ).to.equal(false);
    });

    it('should invalidate an expression with consecutive operators', () => {
        expect( isExpressionValid('+') ).to.equal(false);
        expect( isExpressionValid('+-') ).to.equal(false);
        expect( isExpressionValid(' +-') ).to.equal(false);
        expect( isExpressionValid('+ -') ).to.equal(false);
        expect( isExpressionValid('+- ') ).to.equal(false);
        expect( isExpressionValid('1++') ).to.equal(false);
        expect( isExpressionValid(' 1++') ).to.equal(false);
        expect( isExpressionValid('1 ++') ).to.equal(false);
        expect( isExpressionValid('1+ +') ).to.equal(false);
        expect( isExpressionValid('1++ ') ).to.equal(false);
        expect( isExpressionValid('1+2++') ).to.equal(false);
        expect( isExpressionValid(' 1+2++') ).to.equal(false);
        expect( isExpressionValid('1 +2++') ).to.equal(false);
        expect( isExpressionValid('1+ 2++') ).to.equal(false);
        expect( isExpressionValid('1+2 ++') ).to.equal(false);
        expect( isExpressionValid('1+2+ +') ).to.equal(false);
        expect( isExpressionValid('1+2++ ') ).to.equal(false);
    });

    it('should ignore space in an expression', () => {
        expect( isExpressionValid('1+2') ).to.equal(true);
        expect( isExpressionValid(' 1+2') ).to.equal(true);
        expect( isExpressionValid('1 +2') ).to.equal(true);
        expect( isExpressionValid('1+ 2') ).to.equal(true);
        expect( isExpressionValid('1+2 ') ).to.equal(true);
        expect( isExpressionValid(' 1 +2') ).to.equal(true);
        expect( isExpressionValid(' 1 + 2') ).to.equal(true);
        expect( isExpressionValid(' 1 + 2 ') ).to.equal(true);

        expect( isExpressionValid('1-2') ).to.equal(true);
        expect( isExpressionValid(' 1-2') ).to.equal(true);
        expect( isExpressionValid('1 -2') ).to.equal(true);
        expect( isExpressionValid('1- 2') ).to.equal(true);
        expect( isExpressionValid('1-2 ') ).to.equal(true);
        expect( isExpressionValid(' 1 -2') ).to.equal(true);
        expect( isExpressionValid(' 1 - 2') ).to.equal(true);
        expect( isExpressionValid(' 1 - 2 ') ).to.equal(true);

        expect( isExpressionValid('1*2') ).to.equal(true);
        expect( isExpressionValid(' 1*2') ).to.equal(true);
        expect( isExpressionValid('1 *2') ).to.equal(true);
        expect( isExpressionValid('1* 2') ).to.equal(true);
        expect( isExpressionValid('1*2 ') ).to.equal(true);
        expect( isExpressionValid(' 1 *2') ).to.equal(true);
        expect( isExpressionValid(' 1 * 2') ).to.equal(true);
        expect( isExpressionValid(' 1 * 2 ') ).to.equal(true);

        expect( isExpressionValid('1/2') ).to.equal(true);
        expect( isExpressionValid(' 1/2') ).to.equal(true);
        expect( isExpressionValid('1 /2') ).to.equal(true);
        expect( isExpressionValid('1/ 2') ).to.equal(true);
        expect( isExpressionValid('1/2 ') ).to.equal(true);
        expect( isExpressionValid(' 1 /2') ).to.equal(true);
        expect( isExpressionValid(' 1 / 2') ).to.equal(true);
        expect( isExpressionValid(' 1 / 2 ') ).to.equal(true);
    });

    it('should return the root node\'s key when the expression is a single operand', () => {
        expect( parseAndEvaluate('123') ).to.equal(123);
    });

    it('should determine is an expression is complete', () => {
        expect( isExpressionComplete('123') ).to.equal(true);
        expect( isExpressionComplete('1 + 2') ).to.equal(true);
        // expect( isExpressionComplete('+') ).to.equal(false);
        // expect( isExpressionComplete('+ - * / ^') ).to.equal(false);
        expect( isExpressionComplete('1 + ') ).to.equal(false);
        expect( isExpressionComplete('1 + 2 -') ).to.equal(false);
        expect( isExpressionComplete('1 + 2 - 3 *') ).to.equal(false);
        expect( isExpressionComplete('1 + 2 - 3 * 4 /') ).to.equal(false);
        expect( isExpressionComplete('1 + 2 - 3 * 4 / 5 ^') ).to.equal(false);
        expect( isExpressionComplete('(1 + ) - 3 * 4 / 5 ^ 6') ).to.equal(false);
        expect( isExpressionComplete('1 + (2 - ) * 4 / 5 ^ 6') ).to.equal(false);
        expect( isExpressionComplete('1 + 2 - (3 * ) / 5 ^ 6') ).to.equal(false);
        expect( isExpressionComplete('1 + 2 - 3 * (4 / ) ^ 6') ).to.equal(false);
        expect( isExpressionComplete('1 + 2 - 3 * 4 / (5 ^ )') ).to.equal(false);
    });

    it('should treat incomplete expression as valid', () => {
        expect( isExpressionValidButIncomplete('1 + ') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + 2 -') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + 2 - 3 *') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + 2 - 3 * 4 /') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + 2 - 3 * 4 / 5 ^') ).to.equal(false);
        expect( isExpressionValidButIncomplete('(1 + ) - 3 * 4 / 5 ^ 6') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + (2 - ) * 4 / 5 ^ 6') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + 2 - (3 * ) / 5 ^ 6') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + 2 - 3 * (4 / ) ^ 6') ).to.equal(false);
        expect( isExpressionValidButIncomplete('1 + 2 - 3 * 4 / (5 ^ )') ).to.equal(false);
    });

});
