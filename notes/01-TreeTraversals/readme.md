![The Euler tour](../assets/traversalEuler.bmp)

## 1. Breadth-First Traversal

### 1.1 Level Order
[eyeball] visit nodes by levels from top to bottom and from left to right;

[recursion] visit parent and then children *i.e.* `E D B A G F K H C J`

- useful for rendering a graphical representation of the tree
- useful for calculating a node's percentage of inheritance of its parent


## 2. Depth-First Traversal
[eyeball] follow the The Euler tour - treat edge as wall, and see below for various orders

### 2.1 In Order Traversal
[eyeball] read node when bottom side (![2E908F](../assets/2E908F.png)) of node is visited;

[recursion] visit the left child, then the parent and the right child *i.e.* `H A C D J G E F B K`

- overall walks from child to parent, and from left to right
- useful for implementing sorting algorithms such as *Binary Sorting Tree*
- useful for visualising the execution order of an *Expression Tree*

### 2.2 Pre Order Traversal
[eyeball] read node when left side (![0B24FB](../assets/0B24FB.png)) of node is visited;

[recursion] visit the parent first and then left and right children *i.e.* `E D A H C G J B F K`

- overall walks from parent to child, and from left to right
- useful for serialising a tree for storing and copying purposes

### 2.3 Post Order Traversal
[eyeball] read node when right side (![FC2B37](../assets/FC2B37.png)) of node is visited;

[recursion] visit left child, then the right child and then the parent, *i.e.* `H C A J G D F K B E`

- overall walks from child to parent, and from left to right
- useful for removing tree structure to release resources
- useful for executing instructions stored in an *Expression Tree*

[ref](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Trees/trees.html)

## 3. Run TypeScript Demo

```shellscript
# output

tree height: 4
level-order traversal: E,D,B,A,G,F,K,H,C,J
pre-order traversal: E,D,A,H,C,G,J,B,F,K
in-order traversal: H,A,C,D,J,G,E,F,B,K
post-order traversal: H,C,A,J,G,D,F,K,B,E

serialize root with pre-order traversal: [ { id: 7, key: 'E' },
  { id: 4, key: 'D' },
  { id: 2, key: 'A' },
  { id: 1, key: 'H' },
  { id: 3, key: 'C' },
  { id: 6, key: 'G' },
  { id: 5, key: 'J' },
  { id: 9, key: 'B' },
  { id: 8, key: 'F' },
  { id: 10, key: 'K' } ]

tree clone height: 4
level-order traversal: E,D,B,A,G,F,K,H,C,J
pre-order traversal: E,D,A,H,C,G,J,B,F,K
in-order traversal: H,A,C,D,J,G,E,F,B,K
post-order traversal: H,C,A,J,G,D,F,K,B,E

```
