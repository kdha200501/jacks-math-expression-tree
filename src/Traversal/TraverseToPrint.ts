import {TraverseToExportArray} from "./TraverseToExportArray";
import {NullableNode} from "../Node/NullableNode";

export class TraverseToPrint<T, K extends NullableNode<T>> extends TraverseToExportArray<T, K>{
    constructor() {
        super();
    }

    // Public Methods
    public printLevelOrder(): void {
        console.log(`level-order traversal: ${super.exportLevelOrder().map(node => node.key)}`);
    }

    public printPreOrder(): void {
        console.log(`pre-order traversal: ${super.exportPreOrder().map(node => node.key)}`);
    }

    public printInOrder(): void {
        console.log(`in-order traversal: ${super.exportInOrder().map(node => node.key)}`);
    }

    public printPostOrder(): void {
        console.log(`post-order traversal: ${super.exportPostOrder().map(node => node.key)}`);
    }
}
