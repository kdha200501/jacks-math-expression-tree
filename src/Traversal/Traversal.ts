import {NullableNode} from "../Node/NullableNode";
import {Tree} from "../Tree/Tree";

export abstract class Traversal<T, K extends NullableNode<T>> {

    protected constructor() {
    }

    // Private Variables
    private _tree: Tree<T, K>;

    // Private Methods
    private traverseLevelOrderRec(node: K, level: number, ...argList): void {
        if(node.isNull) {
            return;
        }

        if(level === 0) {
            this.onTraverseLevelOrderRec(node, ...argList);
        }
        // endIf desired level has not been reached

        else if(level > 0) {
            this.traverseLevelOrderRec(<K>node.left, level - 1, ...argList);
            this.traverseLevelOrderRec(<K>node.right, level - 1, ...argList);
        }
        // endIf desired level has been reached
    }

    private traversePreOrderRec(node: K, ...argList): void {
        if(node.isNull) {
            return;
        }
        this.onTraversePreOrderRec(node, ...argList);
        this.traversePreOrderRec(<K>node.left, ...argList);
        this.traversePreOrderRec(<K>node.right, ...argList);
    }

    private traverseInOrderRec(node: K, ...argList): void {
        if(node.isNull) {
            return;
        }
        this.traverseInOrderRec(<K>node.left, ...argList);
        this.onTraverseInOrderRec(node, ...argList);
        this.traverseInOrderRec(<K>node.right, ...argList);
    }

    private traversePostOrderRec(node: K, ...argList): void {
        if(node.isNull) {
            return;
        }
        this.traversePostOrderRec(<K>node.left, ...argList);
        this.traversePostOrderRec(<K>node.right, ...argList);
        this.onTraversePostOrderRec(node, ...argList);
    }

    // Abstract Methods
    protected abstract onTraverseLevelOrderRec(node: K, ...argList);
    protected abstract onTraversePreOrderRec(node: K, ...argList);
    protected abstract onTraverseInOrderRec(node: K, ...argList);
    protected abstract onTraversePostOrderRec(node: K, ...argList);

    // Protected Methods
    protected traverseLevelOrder(...argList): void {
        let height = this.tree.height;
        for(let i = 0; i < height; i++) {
            this.traverseLevelOrderRec(this.tree.root, i, ...argList);
        }
    }

    protected traversePreOrder(...argList): void {
        this.traversePreOrderRec(this.tree.root, ...argList);
    }

    protected traverseInOrder(...argList): void {
        this.traverseInOrderRec(this.tree.root, ...argList);
    }

    protected traversePostOrder(...argList): void {
        this.traversePostOrderRec(this.tree.root, ...argList);
    }

    // Accessors
    get tree(): Tree<T, K> {
        return this._tree;
    }

    set tree(tree: Tree<T, K>) {
        this._tree = tree;
    }
}
