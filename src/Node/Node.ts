export abstract class Node<T> {

    protected constructor(protected _key: T) {}

    // Protected Variables
    protected _left: Node<T>;
    protected _right: Node<T>;

    // Accessors
    public abstract key: T;
    public abstract left: Node<T>;
    public abstract right: Node<T>;
}
