import {NullableNode} from "./NullableNode";

export abstract class SerializableNode<T> extends NullableNode<T>{
    protected constructor(key?: T) {
        super(key);
    }

    // Private Variables
    private _id: number;

    // Accessors
    get id(): number {
        return this._id;
    }

    set id(id: number) {
        this._id = id;
    }
}
