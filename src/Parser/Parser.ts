import {NullableNode} from "../Node/NullableNode";
import {Tree} from "../Tree/Tree";
import {Stack} from "../lib/Stack";

/*
    T - the type of a generic NullableNode's key
    K - a generic NullableNode
    L - a generic Tree
*/
export abstract class Parser<T, K extends NullableNode<T>, L extends Tree<T, K>> {
    protected constructor(private _treeClass: {new(): L}) {
    }

    // Protected Abstract Variables
    protected abstract openParenthesis: string;
    protected abstract closeParenthesis: string;

    // Protected Abstract Methods
    protected abstract buildOperatorNode(operator: string, nodeStack: Stack<K>): void;
    protected abstract buildOperandNode(token: string, nodeStack: Stack<K>): void;
    protected abstract splitToken(expressionStr: string): Array<string>;
    protected abstract tokenIsAnOperator(token: string): boolean;
    protected abstract tokenIsLowerPrecedence(a: string, b: string): boolean;

    // Public Methods
    public parseInfixExpression(expression: string): L {
        let tokenList: Array<string> = this.splitToken(expression);// mix of operators and operands

        let operatorTokenStack = new Stack<string>();// buffer for converting infix to postfix convention
        let nodeStack = new Stack<K>();// buffer for building binary tree

        for(let token of tokenList) {
            if(token === this.openParenthesis) {
                operatorTokenStack.push(token);
            }
            // endIf token is sub-expression opening

            else if(token === this.closeParenthesis) {
                // flush out all the operators within the sub-expression, leaving the opening parenthesis
                while( !operatorTokenStack.isEmpty && operatorTokenStack.peek() !== this.openParenthesis ) {
                    this.buildOperatorNode( operatorTokenStack.pop(), nodeStack );
                }
                if( !operatorTokenStack.isEmpty && operatorTokenStack.peek() !== this.openParenthesis ) {
                    nodeStack = null;
                    break;
                }
                // endIf the remaining opening parenthesis is missing
                operatorTokenStack.pop();
            }
            // endIf token is sub-expression closing

            else if( this.tokenIsAnOperator(token) ) {
                // flush out all the operators that are higher in weight
                while( !operatorTokenStack.isEmpty && this.tokenIsLowerPrecedence(token, operatorTokenStack.peek()) ) {
                    this.buildOperatorNode( operatorTokenStack.pop(), nodeStack );
                }
                operatorTokenStack.push(token);
            }
            // endIf token is an operator

            else {
                this.buildOperandNode(token, nodeStack);
            }
            // endIf token is an operand

        }
        // endFor each token in the expression

        // flush out remaining operators
        while(!operatorTokenStack.isEmpty) {
            this.buildOperatorNode( operatorTokenStack.pop(), nodeStack );
        }

        if(nodeStack === null) {
            return null;
        }

        let tree = new this._treeClass();
        tree.root = nodeStack.pop();
        return tree;
    }
}
