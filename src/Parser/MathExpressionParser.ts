import {Parser} from "./Parser";
import {MathOperationEnum, MathOperationWeight, tokenRegExp} from "../Expression/MathExpression";
import {MathExpressionNode} from "../Node/MathExpressionNode";
import {MathExpressionTree} from "../Tree/MathExpressionTree";
import {Stack} from "../lib/Stack";
import {Alphanumeric} from "../Node/AlphanumericNode";

const filterNonEmptyToken = (token: string) => !!token;
const regExpNonDigit = /[^0-9]+/;
const validateFloat = tokenList => tokenList.length > 2 ? false : !tokenList.reduce( (op, item) => op || regExpNonDigit.test(item), false );
const isFloat = token => validateFloat( token.trim().split('.') );

export class MathExpressionParser extends Parser<Alphanumeric, MathExpressionNode, MathExpressionTree> {
    constructor() {
        super(MathExpressionTree);
    }

    // Implement Protected Abstract Variables
    protected get openParenthesis(): string {
        return MathOperationEnum.OPEN_PARENTHESIS;
    }

    protected get closeParenthesis(): string {
        return MathOperationEnum.CLOSE_PARENTHESIS;
    }

    // Implement Abstract Methods
    protected buildOperatorNode(operator: string, nodeStack: Stack<MathExpressionNode>): void {
        // the top node of an expression sub-tree must be an operator
        let node = new MathExpressionNode(operator);
        node.right = nodeStack.pop();
        node.left = nodeStack.pop();
        nodeStack.push(node);
    }

    protected buildOperandNode(token: string, nodeStack: Stack<MathExpressionNode>): void {
        let operand = isFloat(token) ? parseFloat(token) : NaN;
        nodeStack.push( new MathExpressionNode(operand) );
    }

    protected splitToken (expressionStr: string): Array<string> {
        return expressionStr.split(tokenRegExp).filter(filterNonEmptyToken);
    };

    protected tokenIsAnOperator(token: string): boolean {
        return !!MathOperationWeight[token];
    }

    protected tokenIsLowerPrecedence(a: string, b: string): boolean {
        return MathOperationWeight[a] <= MathOperationWeight[b];
    }
}
