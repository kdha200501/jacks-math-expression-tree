import {Tree} from "./Tree";
import {Alphanumeric} from "../Node/AlphanumericNode";
import {MathExpressionNode} from "../Node/MathExpressionNode";
import {TraverseToEvaluateMathExpression} from "../Traversal/TraverseToEvaluateMathExpression";

export class MathExpressionTree extends Tree<Alphanumeric, MathExpressionNode> {

    public constructor() {
        super(MathExpressionNode);

        this._traverseToEvaluateMathExpression = new TraverseToEvaluateMathExpression();
        this._traverseToEvaluateMathExpression.tree = this;
    }

    // Private Variables
    private _traverseToEvaluateMathExpression: TraverseToEvaluateMathExpression;

    // Accessors
    public get traverseToEvaluateMathExpression(): TraverseToEvaluateMathExpression {
        return this._traverseToEvaluateMathExpression;
    }
}
